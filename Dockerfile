FROM redmine:passenger
RUN apt-get update && \
        apt-get install -y build-essential && \
        gem install rufus-scheduler
COPY Gemfile.local /usr/src/redmine/
